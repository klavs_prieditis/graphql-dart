import "package:graphql_dart/language.dart";
import "package:source_span/source_span.dart";

main() {
  final SourceFile sourceFile = SourceFile.fromString(
    """
    type Foo {
      bar: Bar
      baz: [Baz!]!
    }

    type Bar {
      boo: Boo
    }
    """,
  );

  final DocumentNode document = parse(sourceFile);

  final DocumentNode transformedDocument = transform(
    document,
    [
      MyTransformer(),
      MyOtherTransformer(),
    ],
  );

  final String printed = printNode(transformedDocument);

  print(printed);
}

class MyTransformer extends TransformingVisitor {
  @override
  visitObjectTypeDefinitionNode(ObjectTypeDefinitionNode node) =>
      ObjectTypeDefinitionNode(
        description: node.description,
        interfaces: node.interfaces,
        directives: node.directives,
        name: node.name,
        fields: node.fields.followedBy([
          FieldDefinitionNode(
            name: NameNode(value: "myExtraField"),
            type: NamedTypeNode(
              isNonNull: true,
              name: NameNode(value: "MyExtraFieldType"),
            ),
          )
        ]).toList(),
      );
}

class MyOtherTransformer extends TransformingVisitor {
  @override
  visitObjectTypeDefinitionNode(ObjectTypeDefinitionNode node) =>
      ObjectTypeDefinitionNode(
        description: node.description,
        interfaces: node.interfaces,
        directives: node.directives,
        name: node.name,
        fields: node.fields.followedBy([
          FieldDefinitionNode(
            name: NameNode(value: "myOtherExtraField"),
            type: NamedTypeNode(
              isNonNull: true,
              name: NameNode(value: "MyOtherExtraFieldType"),
            ),
          )
        ]).toList(),
      );
}
