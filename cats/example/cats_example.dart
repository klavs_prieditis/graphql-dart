import 'dart:io';

import 'package:yaml/yaml.dart';

import 'package:cats/cats.dart';

main() async {
  final src = await File("./graphql-cats/scenarios/parsing/SchemaParser.yaml")
      .readAsString();

  final builder = CatBuilder();

  final yaml = loadYaml(src);

  final scenario = builder.buildScenario(yaml);

  final runner = CatRunner();

  runner.runScenario(scenario);
}
